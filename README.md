# Generated objects

3D models of objects generated according to specific functional requirements (affordances).  
This is support material for the paper "Automatic generation of object shapes with desired affordances using voxelgrid representation".

The .binvox (binary voxelgrid) models can be viewed using [Viewvox](https://www.patrickmin.com/viewvox/).  
The .stl (mesh) models can be viewed directly in gitlab, or using tools like [MeshLab](http://www.meshlab.net/) or [3dviewer.net](https://3dviewer.net/).

Contact: mihai@andries.eu